package com.devcamp.task6430.drink.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task6430.drink.model.CDrink;
import com.devcamp.task6430.drink.repository.IDrinkRepository;

@RestController
@CrossOrigin
public class DrinkController {
    @Autowired
    private IDrinkRepository iDrinkRepository;

    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getAllDrinks() {
        try {
            List<CDrink> drinkList = new ArrayList<>();
            iDrinkRepository.findAll().forEach(drinkList::add);
            return new ResponseEntity<List<CDrink>>(drinkList, HttpStatus.OK);
        } catch (Exception e) {
           return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    }

    @GetMapping("/drinks/details/{id}")
    public CDrink getDrinkById(@PathVariable Long id) {
            if(iDrinkRepository.findById(id).isPresent()){
                return iDrinkRepository.findById(id).get();
            } else{
                return null;
            }
    }

   
    @PostMapping("/drinks/create/{id}")
    public ResponseEntity<CDrink> createDrink(@PathVariable("id") Long id, @RequestBody CDrink newDrink) {
        try {
            Optional<CDrink> drinkCheck = iDrinkRepository.findById(id);
            if(drinkCheck.isPresent()){
                return new ResponseEntity<>(drinkCheck .get(), HttpStatus.CREATED);
            } else{
                CDrink createDrink = new CDrink();
                createDrink.setMaNuocUong(newDrink.getMaNuocUong());
                createDrink.setTenNuocUong(newDrink.getTenNuocUong());
                return new ResponseEntity<>(iDrinkRepository.save(newDrink), HttpStatus.OK);
            }
        } catch (Exception e) {
           return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/drinks/update/{id}")
    public ResponseEntity<CDrink> updateDrinkById(@PathVariable("id") Long id, @RequestBody CDrink updateDrink) {
        try {
            Optional<CDrink> drinkCheck = iDrinkRepository.findById(id);
            if(drinkCheck.isPresent()){
                CDrink newDrink = drinkCheck.get();
                newDrink.setMaNuocUong(updateDrink.getMaNuocUong());
                newDrink.setTenNuocUong(updateDrink.getTenNuocUong());
                return new ResponseEntity<CDrink>(iDrinkRepository.save(newDrink), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/drinks/delete/{id}")
    public ResponseEntity<CDrink> deleteDrinkById(@PathVariable("id") Long id) {
        try {
            Optional<CDrink> drinkCheck = iDrinkRepository.findById(id);
            if(drinkCheck.isPresent()){
                CDrink deleteDrink = drinkCheck.get();
                iDrinkRepository.delete(deleteDrink);
             return new ResponseEntity<>(HttpStatus.OK) ;
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
