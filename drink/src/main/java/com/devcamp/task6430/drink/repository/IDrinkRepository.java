package com.devcamp.task6430.drink.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task6430.drink.model.CDrink;

public interface IDrinkRepository extends JpaRepository<CDrink,Long>{
    
}
